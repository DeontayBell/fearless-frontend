function createCard(name, description, pictureUrl, location, start, end) {
    return `
    <div class="card mb-5 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${new Date(start).toLocaleDateString()}-
        ${new Date(end).toLocaleDateString()}</div>
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Response not ok');
          // Figure out what to do when the response is bad
        } else {
        const data = await response.json();

        let index = 0
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name
            const start = details.conference.starts;
            const end = details.conference.ends

            const html = createCard(title, description, pictureUrl, location, start, end);

            const column = document.querySelector(`#col-${index % 3}`);
            column.innerHTML += html;
            index +=1

            }
          }

        }


    //         const detailUrl = `http://localhost:8000${conference.href}`;
    //         const detailResponse = await fetch(detailUrl);
    //         if (detailResponse.ok) {

    //         const details = await detailResponse.json();


    //         const detail = details.conferences[0]
    //         const textTag = document.querySelector('.card-text');
    //         textTag.innerHTML = conference.name;
    //   }


      } catch (error) {
            console.error('error', error);
        // Figure out what to do if an error is raised
      }

    });



//     const response = await fetch(url);
//     console.log(response);

//     const data = await response.json();
//     console.log(data);


// });
